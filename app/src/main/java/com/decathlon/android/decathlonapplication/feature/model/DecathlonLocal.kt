package com.decathlon.android.decathlonapplication.feature.model

import com.decathlon.android.decathlonapplication.core.extensions.performOnBack
import com.decathlon.android.decathlonapplication.core.networking.Scheduler
import com.decathlon.android.decathlonapplication.feature.local.CustomerDb
import com.decathlon.android.decathlonapplication.feature.local.Sports
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single

class DecathlonLocal(private val customerDb: CustomerDb, private val scheduler: Scheduler):DecathlonDataContract.Local{

    override fun getTags():List<String>{
        return listOf("Tag1", "Tag2", "Tag3")
    }

    override fun insertCustomerSport(sport: List<Sports>) {
        Completable.fromAction {
            customerDb.customerDao().insertAll(sport)
        }.performOnBack(scheduler).subscribe()
    }

    override fun getSports(): Single<SportsResponse> {
        return Single.just(getSportsResponse())
    }

    private fun getSportsResponse():SportsResponse{
        var res ="{\"sportList\":[{\"SportName\":\"Field hockey\",\"SportId\":\"12\",\"SportExpertices\":\"3\"},{\"SportName\":\"Football\",\"SportId\":\"11\",\"SportExpertices\":\"2\"},{\"SportName\":\"BasketBall\",\"SportId\":\"10\",\"SportExpertices\":\"2\"},{\"SportName\":\"Formula1\",\"SportId\":\"13\",\"SportExpertices\":\"3\"},{\"SportName\":\"Cricket\",\"SportId\":\"14\",\"SportExpertices\":\"2\"},{\"SportName\":\"Ice Hockey\",\"SportId\":\"15\",\"SportExpertices\":\"2\"},{\"SportName\":\"Badminton\",\"SportId\":\"16\",\"SportExpertices\":\"3\"},{\"SportName\":\"Swimming\",\"SportId\":\"17\",\"SportExpertices\":\"2\"},{\"SportName\":\"Cycling\",\"SportId\":\"19\",\"SportExpertices\":\"2\"}]}"
        var gson = Gson()
        return  gson.fromJson(res, SportsResponse::class.java) as SportsResponse
    }
}