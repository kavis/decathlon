package com.decathlon.android.decathlonapplication.feature.views

import android.app.Fragment
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.decathlon.android.decathlonapplication.R
import com.decathlon.android.decathlonapplication.databinding.LoginFragmentBinding
import com.decathlon.android.decathlonapplication.feature.ComponentProvider
import com.decathlon.android.decathlonapplication.feature.model.State
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModel
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import javax.inject.Inject

class LoginFragment:BaseFragment(){

    private val mDecathlonComponent by lazy {
        ComponentProvider.getDetaclonComponent()
    }

    @Inject
    lateinit var mViewModelFactory: DecathlonViewModelFactory

    private val mViewModel by lazy {
        mViewModelFactory.decathlonViewModel
    }

    var mBinding : LoginFragmentBinding?=null

    override fun getCustomTag():String{
        return "login_fragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDecathlonComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater!!, R.layout.login_fragment, container, false)
        return mBinding?.root!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding?.button?.setOnClickListener{
            mViewModel?.stateLiveData?.value = State.onStartNowClicked(100)
        }
    }

}