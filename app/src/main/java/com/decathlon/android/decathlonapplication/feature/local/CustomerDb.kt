package com.decathlon.android.decathlonapplication.feature.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [Sports::class], version = 1,exportSchema = false)
abstract class CustomerDb : RoomDatabase() {
    abstract fun customerDao(): CustomerDao
}
