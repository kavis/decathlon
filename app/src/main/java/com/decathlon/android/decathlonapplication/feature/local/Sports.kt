package com.decathlon.android.decathlonapplication.feature.local

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Sports")
data class Sports(@SerializedName("SportId") @PrimaryKey val sportId: Int,
                @SerializedName("SportName") val sportName: String,
                @SerializedName("SportExpertices") val postBody: String)