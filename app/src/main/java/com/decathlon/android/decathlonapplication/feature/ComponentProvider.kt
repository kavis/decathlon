package com.decathlon.android.decathlonapplication.feature

import com.decathlon.android.decathlonapplication.DecathlonApplication
import com.decathlon.android.decathlonapplication.feature.di.DaggerDecathlonComponent
import com.decathlon.android.decathlonapplication.feature.di.DecathlonComponent


object ComponentProvider{
    var decathlonComponent:DecathlonComponent ?= null

    public fun getDetaclonComponent():DecathlonComponent{
        if (decathlonComponent == null){
            decathlonComponent = DaggerDecathlonComponent.builder().coreComponent(DecathlonApplication.coreComponent).build()
        }
        return decathlonComponent!!
    }
}