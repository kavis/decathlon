package com.decathlon.android.decathlonapplication.feature.views

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.decathlon.android.decathlonapplication.R
import com.decathlon.android.decathlonapplication.core.networking.Response
import com.decathlon.android.decathlonapplication.databinding.FavFragmentBinding
import com.decathlon.android.decathlonapplication.feature.ComponentProvider
import com.decathlon.android.decathlonapplication.feature.model.SportsResponse
import com.decathlon.android.decathlonapplication.feature.model.State
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import javax.inject.Inject

class FavoriteSportsFragment : BaseFragment(){

    private val mDecathlonComponent by lazy {
        ComponentProvider.getDetaclonComponent()
    }

    @Inject
    lateinit var mViewModelFactory: DecathlonViewModelFactory

    private val mViewModel by lazy {
        mViewModelFactory.decathlonViewModel
    }

    var mBinding:FavFragmentBinding?=null

    override fun getCustomTag():String{
        return "favorite_fragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDecathlonComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater!!, R.layout.fav_fragment, container, false)
        return mBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel?.getSports()
        mViewModel?.mGetSportsLiveData?.observe(this, Observer<Response<SportsResponse>> {
            when (it){
                is Response.Success -> showTags(it.data)
                is Response.Failure -> showError(it.e)
            }
        })

        mBinding?.button3?.setOnClickListener{
            if (mViewModel?.mSportId?.size!! in 1..3){
                mViewModel?.stateLiveData?.value = State.onFavoriteSelected(100)
            }else {
                Toast.makeText(activity, "Please select sports min 1 and max 3", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showError(e: Throwable) {
        Toast.makeText(activity, e.localizedMessage, Toast.LENGTH_LONG).show()
    }

    private  var mData: SportsResponse?=null

    private fun showTags(data: SportsResponse) {
        mData = data
        var list  = ArrayList<String>()
        for (i in data.sportLis!!){
            list.add(i.sportName!!)
        }
        mBinding?.tagGroup?.setTags(list)
        mBinding?.tagGroup?.setOnTagClickListener { tag->

            if (!mViewModel?.mSportName?.contains(tag)!!) {
                mViewModel?.mSportId?.add(
                        mData!!.sportLis?.last { it.sportName.equals(tag) }?.sportId!!)
                mViewModel?.mSportName?.add(mData!!.sportLis?.last { it.sportName.equals(tag) }?.sportName!!)
            }else {
               var index =  mViewModel!!.mSportName.indexOf(tag)
                mViewModel?.mSportName?.removeAt(index)
                mViewModel?.mSportId?.removeAt(index)
            }
        }
    }


}