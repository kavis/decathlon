package com.decathlon.android.decathlonapplication.feature.di

import android.arch.persistence.room.Room
import android.content.Context
import com.decathlon.android.decathlonapplication.core.Constants
import com.decathlon.android.decathlonapplication.core.di.CoreComponent
import com.decathlon.android.decathlonapplication.core.networking.Scheduler
import com.decathlon.android.decathlonapplication.feature.local.CustomerDao
import com.decathlon.android.decathlonapplication.feature.local.CustomerDb
import com.decathlon.android.decathlonapplication.feature.model.DecathlonDataContract
import com.decathlon.android.decathlonapplication.feature.model.DecathlonLocal
import com.decathlon.android.decathlonapplication.feature.model.DecathlonRepositroy
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import com.decathlon.android.decathlonapplication.feature.views.*

import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@DecathlonScope
@Component(dependencies = [CoreComponent::class], modules = [GithubModule::class, RepoModule::class])
interface DecathlonComponent {
    fun scheduler(): Scheduler
    fun inject(mainActivity: MainActivity)
    fun inject(fragment: LoginFragment)
    fun inject(fragment: FavoriteSportsFragment)
    fun inject(fragment: ExperticesFragment)
    fun inject(fragment: PreviewFragment)
}

@Module
class GithubModule {

    @Provides
    @DecathlonScope
    fun decathlonViewModelFactory(repository: DecathlonDataContract.Repository,compositeDisposable: CompositeDisposable): DecathlonViewModelFactory = DecathlonViewModelFactory(repository,compositeDisposable)

    @Provides
    @DecathlonScope
    fun decathlonRepo(local: DecathlonDataContract.Local, scheduler: Scheduler, compositeDisposable: CompositeDisposable): DecathlonDataContract.Repository = DecathlonRepositroy(local, scheduler, compositeDisposable)


    @Provides
    @DecathlonScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()
}

@Module
class RepoModule {

    @Provides
    @DecathlonScope
    fun localData(customerDb: CustomerDb, scheduler: Scheduler): DecathlonDataContract.Local = DecathlonLocal(customerDb, scheduler)

    @Provides
    @DecathlonScope
    fun customerDb(context: Context): CustomerDb = Room.databaseBuilder(context, CustomerDb::class.java, Constants.DB_NAME).build()
}