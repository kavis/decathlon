package com.decathlon.android.decathlonapplication.feature.local

import android.arch.persistence.room.*
import com.decathlon.android.decathlonapplication.feature.model.SportsResponse
import io.reactivex.Flowable


@Dao
interface CustomerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(sports: List<Sports>)


    @Query("SELECT * FROM Sports")
    fun getAll(): Flowable<List<Sports>>
}