package com.decathlon.android.decathlonapplication.feature.model

import com.decathlon.android.decathlonapplication.DecathlonApplication
import com.decathlon.android.decathlonapplication.core.networking.Response
import com.decathlon.android.decathlonapplication.feature.local.Sports
import com.decathlon.android.decathlonapplication.feature.model.SportsResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface DecathlonDataContract {
    interface Repository {
        val getSportsResponse: PublishSubject<Response<SportsResponse>>
        fun getSports(): PublishSubject<Response<SportsResponse>>
        fun insertCustomerSport(sport: List<Sports>)
        fun getTags():List<String>
    }

    interface Local {
        fun getSports():Single<SportsResponse>
        fun insertCustomerSport(sport:List<Sports>)
        fun getTags():List<String>
    }

}