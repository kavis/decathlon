package com.decathlon.android.decathlonapplication.feature.views

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import com.decathlon.android.decathlonapplication.R
import com.decathlon.android.decathlonapplication.databinding.ExperticesFragmentBinding
import com.decathlon.android.decathlonapplication.databinding.ExpertiseItemBinding
import com.decathlon.android.decathlonapplication.feature.ComponentProvider
import com.decathlon.android.decathlonapplication.feature.model.State
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import javax.inject.Inject

class ExperticesFragment:BaseFragment(){

    private val mDecathlonComponent by lazy {
        ComponentProvider.getDetaclonComponent()
    }

    @Inject
    lateinit var mViewModelFactory: DecathlonViewModelFactory

    var mBinding:ExperticesFragmentBinding?=null

    private val mViewModel by lazy {
        mViewModelFactory.decathlonViewModel
    }

    override fun getCustomTag(): String {
        return "expertices_fragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDecathlonComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater!!, R.layout.expertices_fragment, container, false)
        return mBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding?.recylerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mBinding?.recylerView?.adapter = ExpertiseAdapter()

        mBinding?.next?.setOnClickListener{
            mViewModel?.stateLiveData?.value = State.onExperticesSelected(100)
        }
        for (i in 0..(mViewModel?.mSportName?.size!!)) mViewModel?.mSportExpertices?.add("POOR")
    }


    inner class ExpertiseAdapter(): RecyclerView.Adapter<ExpertiseViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpertiseViewHolder {
            var binding : ExpertiseItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.expertise_item, parent, false)
            return ExpertiseViewHolder(binding)
        }

        override fun getItemCount() = mViewModel?.mSportId?.size!!

        override fun onBindViewHolder(holder: ExpertiseViewHolder, position: Int) {
            holder.binding.sportName.text = mViewModel?.mSportName!![position]
            holder.binding.ratingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { p0, p1, p2 ->

                var rating = "POOR"
                if (p1 > 0 && p1 < 3){
                     rating = "POOR"
                }else if (p1 >=3 && p1<4){
                    rating = "AVERAGE"
                }else {
                    rating = "EXCELLENT"
                }

                holder.binding.ratingText.text = rating
                mViewModel?.mSportExpertices!![position] = rating
            }
        }
    }

    inner class ExpertiseViewHolder(var binding: ExpertiseItemBinding): RecyclerView.ViewHolder(binding.root)
}