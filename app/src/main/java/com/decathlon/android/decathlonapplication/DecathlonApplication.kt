package com.decathlon.android.decathlonapplication

import android.app.Application
import com.decathlon.android.decathlonapplication.core.di.AppModule
import com.decathlon.android.decathlonapplication.core.di.CoreComponent
import com.decathlon.android.decathlonapplication.core.di.DaggerCoreComponent

class DecathlonApplication : Application(){
    companion object {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDI()
    }


    private fun initDI() {
        coreComponent = DaggerCoreComponent.builder().appModule(AppModule(this)).build()
    }
}