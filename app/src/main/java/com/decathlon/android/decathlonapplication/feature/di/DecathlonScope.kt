package com.decathlon.android.decathlonapplication.feature.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DecathlonScope