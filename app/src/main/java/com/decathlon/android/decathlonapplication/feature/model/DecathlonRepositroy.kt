package com.decathlon.android.decathlonapplication.feature.model

import com.decathlon.android.decathlonapplication.core.extensions.*
import com.decathlon.android.decathlonapplication.core.networking.Response
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import com.decathlon.android.decathlonapplication.core.networking.Scheduler
import com.decathlon.android.decathlonapplication.feature.local.Sports

class DecathlonRepositroy(private val local:DecathlonDataContract.Local, private val scheduler: Scheduler,
                          private val compositeDisposable: CompositeDisposable):DecathlonDataContract.Repository{

    override fun getTags():List<String> = local.getTags()

    override fun insertCustomerSport(sport: List<Sports>) {
        local.insertCustomerSport(sport)
    }

    override val getSportsResponse: PublishSubject<Response<SportsResponse>> = PublishSubject.create()


    override fun getSports(): PublishSubject<Response<SportsResponse>> {
        getSportsResponse.loading(true)
        local.getSports()
                .performOnBackOutOnMain(scheduler)
                .subscribe({
                    getSportsResponse.success(it)
                },{
                    getSportsResponse.failed(it)
                }).addTo(compositeDisposable)
        return getSportsResponse
    }

}