package com.decathlon.android.decathlonapplication.core

object Constants {
    val API_URL = "https://api.github.com"
    val DB_NAME: String = "sports.db"
}