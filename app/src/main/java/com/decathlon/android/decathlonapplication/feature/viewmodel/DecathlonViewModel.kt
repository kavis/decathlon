package com.decathlon.android.decathlonapplication.feature.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.net.Uri
import com.decathlon.android.decathlonapplication.core.extensions.toLiveData
import com.decathlon.android.decathlonapplication.core.networking.Response
import com.decathlon.android.decathlonapplication.feature.local.Sports
import com.decathlon.android.decathlonapplication.feature.model.DecathlonDataContract
import com.decathlon.android.decathlonapplication.feature.model.SportsResponse
import com.decathlon.android.decathlonapplication.feature.model.State
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class DecathlonViewModel(private val repo: DecathlonDataContract.Repository,
private val compositeDisposable: CompositeDisposable) : ViewModel() {

    var mSportId  = ArrayList<Int>()
    var mSportName =ArrayList<String>()
    var mSportExpertices=ArrayList<String>()

    val mGetSportsLiveData : LiveData<Response<SportsResponse>> by lazy {
        repo.getSportsResponse.toLiveData(compositeDisposable)
    }

    fun getSports() {
        repo.getSports()
    }


    fun saveCustomerData(){
        var list = ArrayList<Sports>()

        if (mSportName?.size > 0) {
            for (i in 0..mSportName?.size!!) {
                list[i] = Sports(mSportId!![i], mSportName!![i], mSportExpertices!![i])
            }
            repo.insertCustomerSport(list)
        }
    }

    val stateLiveData:MutableLiveData<State> = MutableLiveData<State>()
}