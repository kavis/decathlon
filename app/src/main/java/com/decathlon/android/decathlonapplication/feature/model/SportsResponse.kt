package com.decathlon.android.decathlonapplication.feature.model

import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

class SportsResponse{

    @SerializedName("sportList")
    var sportLis : List<Sport> ?= null

    class Sport{

        @SerializedName("SportName")
        var sportName : String ?=null
        @PrimaryKey
        @SerializedName("SportId")
        var sportId : Int?= null
        @SerializedName("SportExpertices")
        var sportExpertices : Int ?= null
    }
}