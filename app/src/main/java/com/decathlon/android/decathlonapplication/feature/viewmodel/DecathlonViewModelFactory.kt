package com.decathlon.android.decathlonapplication.feature.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.decathlon.android.decathlonapplication.feature.model.DecathlonDataContract
import io.reactivex.disposables.CompositeDisposable


class DecathlonViewModelFactory(private val repository: DecathlonDataContract.Repository, private val compositeDisposable: CompositeDisposable) :
        ViewModelProvider.Factory {

    var decathlonViewModel: DecathlonViewModel ?= null
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (decathlonViewModel == null){
            decathlonViewModel = DecathlonViewModel(repository, compositeDisposable)
        }
        return decathlonViewModel as T
    }
}