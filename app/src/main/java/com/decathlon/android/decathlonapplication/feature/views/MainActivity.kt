package com.decathlon.android.decathlonapplication.feature.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.decathlon.android.decathlonapplication.R
import com.decathlon.android.decathlonapplication.feature.ComponentProvider
import com.decathlon.android.decathlonapplication.feature.model.State
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModel
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private val mDecathlonComponent by lazy {
        ComponentProvider.getDetaclonComponent()
    }

    @Inject
    lateinit var mViewModelFactory: DecathlonViewModelFactory

    private val mViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(DecathlonViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDecathlonComponent.inject(this)

        mViewModel.stateLiveData.observe(this, Observer<State> {
            when (it){
                is State.Favorite -> showFavorite()
                is State.Expertices -> showExpertices()
                is State.Preview -> showPreview()
            }
        })

        showWelcome()
    }

    private fun showPreview() {
        showFragment(PreviewFragment())
    }

    private fun showExpertices() {
        showFragment(ExperticesFragment())
    }

    private fun showFavorite() {

        showFragment(FavoriteSportsFragment())
    }

    private fun showWelcome(){
        showFragment(LoginFragment())
    }

    private fun showFragment(fragment:BaseFragment){
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.emptyLayout, fragment)
        ft.addToBackStack(fragment.tag)
        ft.commit()
    }


}
