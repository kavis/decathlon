package com.decathlon.android.decathlonapplication.feature.model


sealed class State {
    data class Start(var state:Int) : State()
    data class Favorite(var state:Int) : State()
    data class Expertices(var state:Int) : State()
    data class Preview(var state:Int) : State()

    companion object {
        fun  onStartNowClicked(state: Int): State = Favorite(state)
        fun  onFavoriteSelected(state: Int): State = Expertices(state)
        fun  onExperticesSelected(state: Int): State = Preview(state)
    }
}