package com.decathlon.android.decathlonapplication.feature.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import com.decathlon.android.decathlonapplication.R
import com.decathlon.android.decathlonapplication.databinding.ExpertiseItemBinding
import com.decathlon.android.decathlonapplication.databinding.PreviewFragmentBinding
import com.decathlon.android.decathlonapplication.databinding.PreviewItemBinding
import com.decathlon.android.decathlonapplication.feature.ComponentProvider
import com.decathlon.android.decathlonapplication.feature.viewmodel.DecathlonViewModelFactory
import javax.inject.Inject

class PreviewFragment:BaseFragment(){

    private val mDecathlonComponent by lazy {
        ComponentProvider.getDetaclonComponent()
    }

    @Inject
    lateinit var mViewModelFactory: DecathlonViewModelFactory

    private val mViewModel by lazy {
        mViewModelFactory.decathlonViewModel
    }

    override fun getCustomTag(): String {
        return "preview_fragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDecathlonComponent.inject(this)
    }


    private var mBinding: PreviewFragmentBinding?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater!!, R.layout.preview_fragment, container, false)
        return mBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding?.recylerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mBinding?.recylerView?.adapter = PreviewAdapter()
        mBinding?.button2?.setOnClickListener{
            mViewModel?.saveCustomerData()
        }
    }

    inner class PreviewAdapter(): RecyclerView.Adapter<PreviewViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PreviewViewHolder {
            var binding : PreviewItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.preview_item, parent, false)
            return PreviewViewHolder(binding)
        }

        override fun getItemCount() = mViewModel?.mSportId?.size!!

        override fun onBindViewHolder(holder: PreviewViewHolder, position: Int) {
           holder?.binding?.textView5?.text = mViewModel?.mSportName!![position]
            holder?.binding?.textView6?.text = mViewModel?.mSportExpertices!![position]
        }
    }

    inner class PreviewViewHolder(var binding: PreviewItemBinding): RecyclerView.ViewHolder(binding.root)

}